import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import Routes from './routes'
import App from './App.vue'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import locale from 'iview/dist/locale/ru-RU'
import global from './global'

Vue.filter('pluralize', global.pluralize)

Vue.use(iView, { locale })
Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(Vuex)

if(localStorage.getItem('token'))
{
	Vue.http.headers.common['Authorization'] = 'Token ' + localStorage.getItem('token');
}

const userModule = {
	state: {
		user: {
			is_authenticated: !!localStorage.getItem('token'),
			username: localStorage.getItem('user_username'),
			last_name: localStorage.getItem('user_last_name'),
			name: localStorage.getItem('user_name'),
			surname: localStorage.getItem('user_surname'),
			coins: localStorage.getItem('user_coins'),
			cristals: localStorage.getItem('user_cristals'),
			rubles: localStorage.getItem('user_rubles'),
			notifications_count: localStorage.getItem('notifications_count'),
		}
	},
	mutations: {
		LOGIN (state, response) {
			Vue.http.headers.common['Authorization'] = 'Token ' + response.token;
			state.user.username = response.user.username;
			state.user.last_name = response.user.last_name;
			state.user.name = response.user.name;
			state.user.surname = response.user.surname;
			state.user.coins = response.user.coins;
			state.user.cristals = response.user.cristals;
			state.user.rubles = response.user.rubles;
			state.user.notifications_count = response.user.notifications_count;
			localStorage.setItem('token', response.token);
			localStorage.setItem('user_username', response.user.username);
			localStorage.setItem('user_name', response.user.name);
			localStorage.setItem('user_last_name', response.user.last_name);
			localStorage.setItem('user_surname', response.user.surname);
			localStorage.setItem('user_coins', response.user.coins);
			localStorage.setItem('user_cristals', response.user.cristals);
			localStorage.setItem('user_rubles', response.user.rubles);
			localStorage.setItem('notifications_count', response.user.notifications_count);
			state.user.is_authenticated = true;
		},
		LOGOUT (state) {
			delete Vue.http.headers.common['Authorization'];
			state.user.username = null;
			state.user.last_name = null;
			state.user.name = null;
			state.user.surname = null;
			state.user.coins = null;
			state.user.cristals = null;
			state.user.rubles = null;
			state.user.notifications_count = 0;
			localStorage.removeItem('token');
			localStorage.removeItem('user_username');
			localStorage.removeItem('user_name');
			localStorage.removeItem('user_last_name');
			localStorage.removeItem('user_surname');
			localStorage.removeItem('user_coins');
			localStorage.removeItem('user_cristals');
			localStorage.removeItem('user_rubles');
			localStorage.removeItem('notifications_count');
			state.user.is_authenticated = false;
		},
		INITIAL (state, user_obj) {
			if (state.user.is_authenticated && user_obj == null){
				delete Vue.http.headers.common['Authorization'];
				state.user.is_authenticated = false;
			}
		},
	}
}

const store = new Vuex.Store({
	state: {
		visit_id: null,
	},
	modules: {
		user_app: userModule,
	}
});

const router = new VueRouter({
	mode: 'history',
	routes: Routes
});

router.beforeEach((to, from, next) => {
	if (to.meta && to.meta.title)
	{
		document.title = to.meta.title;
	}

	next();
})

new Vue({
	el: '#app',
	render: h => h(App),
	router: router,
	store
})
