
if (localStorage.getItem('debug_mode') == null) {
	localStorage.setItem('debug_mode', '1');
}

// variables:
var debug_mode = localStorage.getItem('debug_mode') == "1";
const API_URL = 'http://localhost:8000/';
const LOGIN_URL = API_URL + 'user_app/api_token_auth/';
const INITIAL_URL = API_URL + 'user_app/api_initial/';


// functions:
/** 
 * Pluralizing numbers
 * @param {Number} value (21)
 * @param {Array} arr (['рублей', 'рубль', 'рубля'])
 * @return {String} number + plural form ("21 рубль")
 */
function pluralize(value, arr) {
	if (typeof value !== "number")
	{
		console.error("pluralize value is not number");
		return undefined;
	}
	if (value % 100 == 11 || value % 100 == 12 || value % 100 == 13 || value % 100 == 14)
		return value + " " + arr[0];
	else if (value % 10 == 1)
		return value + " " + arr[1];
	else if (value % 10 == 2 || value % 10 == 3 || value % 10 == 4)
		return value + " " + arr[2];
	return value + " " + arr[0];
}

export default {
	debug_mode: debug_mode,

	// for REST API
	API_URL: API_URL,
	LOGIN_URL: LOGIN_URL,
	INITIAL_URL: INITIAL_URL,
	
	pluralize: pluralize,
	
	initial(context) {
		if (debug_mode) {
			localStorage.setItem('token', "c5a1494c563e5d0cb28dc3153d99bed13449ad89");
			localStorage.setItem('username', "insanecake");
			localStorage.setItem('name', "Антон");
			localStorage.setItem('last_name', "Чубов");
			localStorage.setItem('surname', "");
			localStorage.setItem('coins', 15);
			localStorage.setItem('cristals', 5);
			localStorage.setItem('rubles', 0);
			localStorage.setItem('notifications_count', 0);

			context.$store.commit("LOGIN", null, {module: 'user_app'});

			// Redirect to a specified route
			if(redirect) {
				context.$router.push(redirect);
			}
		}
		else {
			context.$http.post(INITIAL_URL, {url: }).then(response => {
				
				localStorage.setItem('token', response.body.token);
				localStorage.setItem('username', response.body.username);
				localStorage.setItem('name', response.body.name);
				localStorage.setItem('last_name', response.body.last_name);
				localStorage.setItem('surname', response.body.surname);
				localStorage.setItem('coins', response.body.coins);
				localStorage.setItem('cristals', response.body.cristals);
				localStorage.setItem('rubles', response.body.rubles);
				localStorage.setItem('notifications_count', response.body.notifications_count);
				
				context.$store.commit("LOGIN", null, {module: 'user_app'});
				
				// Redirect to a specified route
				if(redirect) {
					context.$router.push(redirect);
				}
			}, response => {
				alert("AJAX error");
				console.log(response);
			});
		}
	},
	
	logout(context) {
		context.$store.commit("LOGOUT", null, {module: 'user_app'});
		context.$Message.success('Вы вышли из аккаунта');
	},
	user_get_short_name(self) {
		var arr = [];
		if (self.name)
			arr.push(self.name);
		if (self.last_name)
			arr.push(self.last_name);
		if (arr.length > 0)
			return arr.join(" ");
		else
			return self.username;
	},
	user_get_full_name(self) {
		var arr = [];
		if (self.last_name)
			arr.push(self.last_name);
		if (self.name)
			arr.push(self.name);
			if (self.surname)
				arr.push(self.surname);
		if (arr.length > 0)
			return arr.join(" ");
		else
			return self.username;
	},
	course_is_free(self) {
		return self.cost_coins == 0 && self.cost_cristals == 0 && self.cost_rubles == 0;
	},
	course_get_cost_string(self) {
		var arr = [];
        if (self.cost_rubles > 0)
            arr.push(pluralize(self.cost_rubles, ["рублей", "рубль", "рубля"]));
        if (self.cost_cristals > 0)
            arr.push(pluralize(self.cost_cristals, ["кристалов", "кристал", "кристала"]));
        if (self.cost_coins > 0)
            arr.push(pluralize(self.cost_coins, ["монет", "монета", "монеты"]));

        if (len(arr) == 3)
            return "за " + arr[0] + ", " + arr[1] + " и " + arr[2];
        else if (len(arr) == 2)
            return "за " + arr[0] + " и " + arr[1];
        else if (len(arr) == 1)
            return "за " + arr[0];
        console.error("cost arr has unknown length");
        return "бесплатно";
	},
}