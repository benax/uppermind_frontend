// === DEFAULT / CUSTOM STYLE ===
// WARNING! always comment out ONE of the two require() calls below.
// 1. use next line to activate CUSTOM STYLE (./src/themes)
require(`./themes/app.${__THEME}.styl`)
// 2. or, use next line to activate DEFAULT QUASAR STYLE
// require(`quasar/dist/quasar.${__THEME}.css`)
// ==============================

// Uncomment the following lines if you need IE11/Edge support
// require(`quasar/dist/quasar.ie`)
// require(`quasar/dist/quasar.ie.${__THEME}.css`)

import Vue from 'vue'
import Vuex from 'vuex'
import Quasar from 'quasar'
import Routes from './routes'
import global from './global'
// import router from './router'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

Vue.config.productionTip = false
Vue.use(Quasar) // Install Quasar Framework
Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(VueResource)

if (__THEME === 'mat') {
  require('quasar-extras/roboto-font')
}
import 'quasar-extras/material-icons'
import 'quasar-extras/ionicons'
import { setInterval } from 'timers';
// import 'quasar-extras/fontawesome'
// import 'quasar-extras/animate'


Vue.filter('pluralize', global.pluralize);
Vue.filter('relative_time', global.relative_time);

if(localStorage.getItem('token'))
{
	Vue.http.headers.common['Authorization'] = 'Token ' + localStorage.getItem('token');
}

function profile_update(state, data) {
	state.user.username = data.username;
	state.user.last_name = data.last_name;
	state.user.name = data.name;
	state.user.surname = data.surname;
	state.user.coins = data.coins;
	state.user.crystals = data.crystals;
	state.user.rubles = data.rubles;
	localStorage.setItem('user_username', data.username);
	localStorage.setItem('user_name', data.name);
	localStorage.setItem('user_last_name', data.last_name);
	localStorage.setItem('user_surname', data.surname);
	localStorage.setItem('user_coins', data.coins);
	localStorage.setItem('user_crystals', data.crystals);
	localStorage.setItem('user_rubles', data.rubles);
}

const userModule = {
	state: {
		user: {
			is_authenticated: !!localStorage.getItem('token'),
			username: localStorage.getItem('user_username'),
			last_name: localStorage.getItem('user_last_name'),
			name: localStorage.getItem('user_name'),
			surname: localStorage.getItem('user_surname'),
			coins: localStorage.getItem('user_coins'),
			crystals: localStorage.getItem('user_crystals'),
			rubles: localStorage.getItem('user_rubles'),
			notifications_count: localStorage.getItem('notifications_count'),
		}
	},
	mutations: {
		LOGIN (state, response) {
			Vue.http.headers.common['Authorization'] = 'Token ' + response.token;
			profile_update(state, response.user);
			state.user.notifications_count = response.user.notifications_count;
			localStorage.setItem('notifications_count', response.user.notifications_count);
			localStorage.setItem('token', response.token);
			state.user.is_authenticated = true;
		},
		LOGOUT (state) {
			delete Vue.http.headers.common['Authorization'];
			state.user.username = null;
			state.user.last_name = null;
			state.user.name = null;
			state.user.surname = null;
			state.user.coins = null;
			state.user.crystals = null;
			state.user.rubles = null;
			state.user.notifications_count = 0;
			localStorage.removeItem('token');
			localStorage.removeItem('user_username');
			localStorage.removeItem('user_name');
			localStorage.removeItem('user_last_name');
			localStorage.removeItem('user_surname');
			localStorage.removeItem('user_coins');
			localStorage.removeItem('user_crystals');
			localStorage.removeItem('user_rubles');
			localStorage.removeItem('notifications_count');
			state.user.is_authenticated = false;
		},
		INITIAL (state, response) {
			if (state.user.is_authenticated && response.user == null) {
				delete Vue.http.headers.common['Authorization'];
				state.user.username = null;
				state.user.last_name = null;
				state.user.name = null;
				state.user.surname = null;
				state.user.coins = null;
				state.user.crystals = null;
				state.user.rubles = null;
				state.user.notifications_count = 0;
				localStorage.removeItem('token');
				localStorage.removeItem('user_username');
				localStorage.removeItem('user_name');
				localStorage.removeItem('user_last_name');
				localStorage.removeItem('user_surname');
				localStorage.removeItem('user_coins');
				localStorage.removeItem('user_crystals');
				localStorage.removeItem('user_rubles');
				localStorage.removeItem('notifications_count');
				state.user.is_authenticated = false;
			}
			else if (response.user !== null) {
				profile_update(state, response.user);
				state.user.notifications_count = response.user.notifications_count;
				localStorage.setItem('notifications_count', response.user.notifications_count);
				state.user.is_authenticated = true;
			}
			if (localStorage.getItem('f_version') == null) { 
				localStorage.setItem('f_version', response.f_version);
			}
			if (response.f_version && localStorage.getItem('f_version') !== null && localStorage.getItem('f_version') !== response.f_version) {
				if (confirm("Доступна новая версия. Обновить?")) {
					localStorage.setItem('f_version', response.f_version);
					location.reload(true);
				}
			}
		},
		SET_NOTIFICATIONS_COUNT (state, value) {
			state.user.notifications_count = value;
			localStorage.setItem('notifications_count', value);
		},
		PROFILE_UPDATE (state, data) {
			profile_update(state, data);
		},
	}
}

// every 500 ms
const ADD_SCROLL = 0,
ADD_RESIZE = 1,
MONITOR_CHANGE = 2,
// 0 - x
// 1 - y
// 2 - button
// 3 - is_down
KEY_PRESS = 3,
// 0 - is_active
// 1 - type (1 - page visible (html api), 2 - potential for 4 sec, 3 - document, 4 - window)
SET_ACTIVE = 4,
SET_MOUSE_MOVE = 5,
// 0 - type (1 - cut, 2 - copy, 3 - paste)
SET_BUFFER = 6,
// get selection length with 500 ms delay
SET_SELECTION = 7,
// get editor selection length with 500 ms delay
SET_EDITOR_SELECTION = 8,
// 0 - keycode
// 1 - ctrl
// 2 - shift
// 3 - alt
CLICK = 9,
/* sample element codes:
1 - menu button
2 - profile menu button
3 - send (main page function)
4 - save (main page function)
*/
ELEMENT_CLICK = 10;

var visit_i = 0, visit_data_i = 0;
var last_s = 0, last_w = 0, last_h = 0, last_sw = 0, last_sh = 0, last_asw = 0 , last_ash = 0;
var last_click_0 = 0, last_click_1 = 0, last_click_2 = 0, last_click_9 = 0;

var visit_record = {
	visit_id: null,
	start_time: new Date().getTime(),
	visits_next: [],
	visit_offset: 0,
	luid: sessionStorage.getItem('luid'),
};

window.VISIT_NEXT = function (visit_data) {
	/* data:
	0 - scrolls
	1 - resizes
	2 - monitor change
	3 - keydowns
	4 - tab active (0 - inactive, 1 - active, 2 - blur, 3 - focus)
	5 - mouse move in quant (was move in last 2 seconds)
	6 - copy / paste / cut (+ length)
	7 - selection
	8 - selection in code
	9 - clicks
	10 - element_click (+ local tab select + task options select)
	ideas: 
		big length change (paste, delete: line, word)
		initial position
	*/
	if (visit_record.visits_next.length == 1 && visit_record.visits_next[0][5].length == 0) {
		visit_record.visits_next.shift();
	}
	else if (visit_record.visits_next.length > 0) {
		// change status for previous visit_next
		visit_record.visits_next[visit_record.visits_next.length - 1][2] = 1;
	}
	visit_record.visits_next.push([
		/*
		0 - id from db
		1 - index
		2 - status (0 - writing, 1 - finished correctly, 2 - before unload)
		3 - time
		4 - routes data [from, to, history length]
		5 - data
		6 - offset
		*/
		null, // 0
		visit_i++, // 1
		0, // 2
		new Date().getTime() - visit_record.start_time, // 3
		[visit_data[0], visit_data[1], window.history.length], // 4
		[], // 5
		0, // 6
	]);
	visit_data_i = 0;
}
VISIT_NEXT([{name: null, params: null}, {name: null, params: null}]);


function add_data(e_code, data = null) {
	if (data != null) {
		visit_record.visits_next[visit_record.visits_next.length - 1][5].push([visit_data_i++, e_code, new Date().getTime() - visit_record.start_time].concat(data));
	}
	else {
		visit_record.visits_next[visit_record.visits_next.length - 1][5].push([visit_data_i++, e_code, new Date().getTime() - visit_record.start_time]);
	}
}


const store = new Vuex.Store({
	state: {
		visit_id: null,
	},
	mutations: {
		SET_VISIT_ID (state, value) {
			visit_record.visit_id = value;
			state.visit_id = value;
		},
	},
	modules: {
		user_app: userModule,
	}
});

function addEvent(obj, evType, fn, isCapturing){
	if (isCapturing==null) isCapturing=false; 
	if (obj.addEventListener){
		// Firefox
		obj.addEventListener(evType, fn, isCapturing);
		return true;
	} else if (obj.attachEvent){
		// MSIE
		var r = obj.attachEvent('on'+evType, fn);
		return r;
	} else {
		return false;
	}
}
addEvent(document, "cut", function(event) {
	add_data(SET_BUFFER, ['1']);
});
addEvent(document, "copy", function(event) {
	add_data(SET_BUFFER, ['2']);
});
addEvent(document, "paste", function(event) {
	add_data(SET_BUFFER, ['3']);
});
addEvent(document, "keypress", function(e) {
	var data = [0, e.ctrlKey ? 1 : 0, e.shiftKey ? 1 : 0, e.altKey ? 1 : 0];
	if (e.which == null) {
		data[0] = e.keyCode;
		add_data(KEY_PRESS, data);
	}
	else if (e.which != 0 && e.charCode != 0) {
		data[0] = e.which;
		add_data(KEY_PRESS, data);
	}
});
addEvent(document, "mousedown", function(e) {
	add_data(CLICK, [e.clientX, e.clientY, e.button, 1]);
	if (e.button == 0) {
		last_click_0 = new Date().getTime();
	}
	else if (e.button == 1) {
		last_click_1 = new Date().getTime();
	}
	else if (e.button == 2) {
		last_click_2 = new Date().getTime();
	}
	else {
		last_click_9 = new Date().getTime();
	}
});
addEvent(document, "mouseup", function(e) {
	if (e.button == 0) {
		if (new Date().getTime() - last_click_0 > 200) {
			add_data(CLICK, [e.clientX, e.clientY, e.button, 0]);
		}
	}
	else if (e.button == 1) {
		if (new Date().getTime() - last_click_1 > 200) {
			add_data(CLICK, [e.clientX, e.clientY, e.button, 0]);
		}
	}
	else if (e.button == 2) {
		if (new Date().getTime() - last_click_2 > 200) {
			add_data(CLICK, [e.clientX, e.clientY, e.button, 0]);
		}
	}
	else {
		if (new Date().getTime() - last_click_9 > 200) {
			add_data(CLICK, [e.clientX, e.clientY, e.button, 0]);
		}
	}
});
addEvent(document, "potentialvisilitychange", function(event) {
	add_data(SET_ACTIVE, [!document.potentialHidden ? 1 : 0, 2]);
});
window.editor_selection_length = 0;
addEvent(document, "editorselectionchange", function(event) {
	alert("TODO: correct data");
	add_data(SET_EDITOR_SELECTION, [window.editor_selection_length]);
});
addEvent(document, "clickonelement", function(event) {
	alert("TODO");
	add_data(ELEMENT_CLICK, []);
});

var last_selection_length = 0;
var curr_selection_length = 0;
var count_wo_selection = 6;

addEvent(document, "selectionchange", function(event) {
	var selection = window.getSelection().toString();
	if (selection) {
		curr_selection_length = selection.length;
	}
	else {
		curr_selection_length = 0;
	}
});
setInterval(function() {
	if (curr_selection_length != last_selection_length) {
		count_wo_selection = 0;
		last_selection_length = curr_selection_length;
	}
	if (count_wo_selection == 5) {
		add_data(SET_SELECTION, [curr_selection_length]);
	}
	count_wo_selection++;
}, 100);

setInterval(function() {
	var s = window.pageYOffset || document.documentElement.scrollTop, 
		w = window.innerWidth || document.body.clientWidth, 
		h = window.innerHeight || document.body.clientHeight,
		sw = screen.width,
		sh = screen.height,
		asw = screen.availWidth,
		ash = screen.availHeight;
	if (last_s != s) {
		add_data(ADD_SCROLL, [s]);
		last_s = s;
	}
	if (last_w != w || last_h != h) {
		add_data(ADD_RESIZE, [w, h]);
		last_w = w;
		last_h = h;
	}
	if (last_sw != sw || last_sh != sh || last_asw != asw || last_ash != ash) {
		add_data(MONITOR_CHANGE, [sw, sh, asw, ash]);
		last_sw = sw;
		last_sh = sh;
		last_asw = asw;
		last_ash = ash;
	}
}, 500);

var was_move = false;
setInterval(function() {
	if (was_move) {
		was_move = false;
		add_data(SET_MOUSE_MOVE);
	}
}, 2000);

var hidden=null;
var visibilityChange=null;
var unifiedVisilityChangeEventDispatchAllowed = true;
if (typeof document.mozHidden !== "undefined") {
	hidden="mozHidden";
	visibilityChange="mozvisibilitychange";
} else if (typeof document.msHidden !== "undefined") {
	hidden="msHidden";
	visibilityChange="msvisibilitychange";
} else if (typeof document.webkitHidden!=="undefined") {
	hidden="webkitHidden";
	visibilityChange="webkitvisibilitychange";
} else if (typeof document.hidden !=="hidden") {
	hidden="hidden";
	visibilityChange="visibilitychange";
}
if (hidden!=null && visibilityChange!=null) {
	addEvent(document, visibilityChange, function(event) {
		add_data(SET_ACTIVE, [document[hidden], 1]);
	});
}


var potentialPageVisibility = {
	pageVisibilityChangeThreshold:3*3600, // in seconds
	init:function() {
		function setAsNotHidden() {
			var dispatchEventRequired=document.potentialHidden;
			document.potentialHidden=false;
			document.potentiallyHiddenSince=0;
			if (dispatchEventRequired) dispatchPageVisibilityChangeEvent();
		}

		function initPotentiallyHiddenDetection() {
			if (!hasFocusLocal) {
				// the window does not has the focus => check for  user activity in the window
				lastActionDate=new Date();
				if (timeoutHandler!=null) {
					clearTimeout(timeoutHandler);
				}
				timeoutHandler = setTimeout(checkPageVisibility, potentialPageVisibility.pageVisibilityChangeThreshold*1000+100); // +100 ms to avoid rounding issues under Firefox
			}
		}

		function dispatchPageVisibilityChangeEvent() {
			unifiedVisilityChangeEventDispatchAllowed=false;
			var evt = document.createEvent("Event");
			evt.initEvent("potentialvisilitychange", true, true);
			document.dispatchEvent(evt);
		}

		function checkPageVisibility() {
			var potentialHiddenDuration=(hasFocusLocal || lastActionDate==null?0:Math.floor((new Date().getTime()-lastActionDate.getTime())/1000));
			document.potentiallyHiddenSince=potentialHiddenDuration;
			if (potentialHiddenDuration>=potentialPageVisibility.pageVisibilityChangeThreshold && !document.potentialHidden) {
				// page visibility change threshold raiched => raise the even
				document.potentialHidden=true;
				dispatchPageVisibilityChangeEvent();
			}
		}

		var lastActionDate=null;
		var hasFocusLocal=true;
		var hasMouseOver=true;
		document.potentialHidden=false;
		document.potentiallyHiddenSince=0;
		var timeoutHandler = null;

		addEvent(document, "pageshow", function(event) {
			add_data(SET_ACTIVE, [1, 3]);
		});
		addEvent(document, "pagehide", function(event) {
			add_data(SET_ACTIVE, [0, 3]);
		});
		addEvent(window, "pageshow", function(event) {
			add_data(SET_ACTIVE, [1, 4]);
		});
		addEvent(window, "pagehide", function(event) {
			add_data(SET_ACTIVE, [0, 4]);
		});
		addEvent(document, "mousemove", function(event) {
			lastActionDate=new Date();
			was_move = true;
		});
		addEvent(document, "mouseover", function(event) {
			hasMouseOver=true;
			setAsNotHidden();
		});
		addEvent(document, "mouseout", function(event) {
			hasMouseOver=false;
			initPotentiallyHiddenDetection();
		});
		addEvent(window, "blur", function(event) {
			hasFocusLocal=false;
			initPotentiallyHiddenDetection();
		});
		addEvent(window, "focus", function(event) {
			hasFocusLocal=true;
			setAsNotHidden();
		});
		setAsNotHidden();
	}
}
potentialPageVisibility.pageVisibilityChangeThreshold = 4;
potentialPageVisibility.init();

var is_loading = false;
setInterval(function() {
	if (!is_loading && visit_record.visit_id !== null) {
		Vue.http.post(global.API_URL + 'user_app/api/visit_track/' + visit_record.visit_id, visit_record).then(response => {
			var splice_i = 0;
			for (let i = 0; i < response.body.visits_next.length; i++) {
				if (response.body.visits_next[i].is_finished) {
					visit_record.visits_next.splice(splice_i, 1);
					splice_i--;
				}
				else {
					visit_record.visits_next[splice_i][0] = response.body.visits_next[i].id;
					visit_record.visits_next[splice_i][5].splice(0, response.body.visits_next[i].count);
				}
				splice_i++;
				if (splice_i == visit_record.visits_next.length) {
					break;
				}
			}
			is_loading = false;
		}, response => {
			is_loading = false;
			console.error('Ошибка с кодом ' + response.status);
		});
	}
}, 5000);

var unsaved_data = localStorage.getItem("unfinished_tracks");
if (unsaved_data) {
	unsaved_data = JSON.parse(unsaved_data);
	for (var k in unsaved_data) {
		Vue.http.post(global.API_URL + 'user_app/api/visit_track/' + k, unsaved_data[k]).then(response => {
			var unsaved_real = JSON.parse(localStorage.getItem("unfinished_tracks"));
			delete unsaved_real[k];
			localStorage.setItem("unfinished_tracks", JSON.stringify(unsaved_real));
		}, response => {
			console.error('Error unsaved sync with code ' + response.status);
		});
	}
}

function saveChanges () {
	if (visit_record.visit_id !== null) {
		var last_data = localStorage.getItem("unfinished_tracks");
		if (last_data) {
			last_data = JSON.parse(last_data);
		}
		else {
			last_data = {};
		}
		visit_record.visits_next[visit_record.visits_next.length - 1][2] = 2;
		last_data[visit_record.visit_id] = visit_record;
		localStorage.setItem("unfinished_tracks", JSON.stringify(last_data));
	}
}
addEvent(window, "beforeunload", function (e) {
	setTimeout( saveChanges, 0 );
});

const router = new VueRouter({
	mode: 'history',
	routes: Routes
});

router.beforeEach((to, from, next) => {
	if (to.meta && to.meta.title)
	{
		document.title = to.meta.title;
	}
	VISIT_NEXT([{name: from.name, params: from.params}, {name: to.name, params: to.params}]);
	next();
})

Quasar.start(() => {
  /* eslint-disable no-new */
  new Vue({
    el: '#q-app',
	router,
	store,
    render: h => h(require('./App').default)
  })
})