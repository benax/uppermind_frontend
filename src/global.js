import { Cookies, Toast } from 'quasar'

// variables:
var debug_mode = localStorage.getItem('debug_mode') == "1";
// const API_URL = localStorage.getItem('local_url') == "1" ? 'http://localhost:8000/' : 'https://api.uppermind.ru/';
const API_URL = 'http://localhost:8000/';
const LOGIN_URL = API_URL + 'user_app/api/token_auth/';
const INITIAL_URL = API_URL + 'user_app/api/initial/';
const LOGOUT_URL = API_URL + 'user_app/api/logout/';

// functions:
/**
 * Pluralizing numbers
 * @param {Number} value (21)
 * @param {Array} arr (['рублей', 'рубль', 'рубля'])
 * @return {String} number + plural form ("21 рубль")
 */
function pluralize(value, arr) {
	if (typeof value !== "number")
	{
		console.error("pluralize value is not number");
		return undefined;
	}
	if (value % 100 >= 11 && value % 100 <= 14)
		return value + " " + arr[0];
	else if (value % 10 == 1)
		return value + " " + arr[1];
	else if (value % 10 >= 2 && value % 10 <= 4)
		return value + " " + arr[2];
	return value + " " + arr[0];
}

/**
 * Humanized relative time
 * @param {String} string_date moment in past
 * @return {String} string or date (** сек. назад)
 */
function relative_time(string_date) {
	var date = Date.parse(string_date);
	var diff = (new Date() - date) / 1000; // difference in seconds

	if (diff < 1) {
		return 'только что';
	}
	var sec = Math.floor(diff);
	if (sec < 60) {
		return pluralize(sec, ['секунд', 'секунду', 'секунды']) + ' назад';
	}
	var min = Math.floor(diff / 60);
	if (min < 60) {
		return pluralize(min, ['минут', 'минуту', 'минуты']) + ' назад';
	}
	var hours = Math.floor(diff / 3600);
	if (hours < 60) {
		return pluralize(hours, ['часов', 'час', 'часа']) + ' назад';
	}
	var days = Math.floor(diff / 86400);
	if (days < 5) {
		return pluralize(days, ['дней', 'день', 'дня']) + ' назад';
	}
	var d = new Date(date);
	var month = Math.floor(diff / 2592000);
	if (month < 5) {
		return d.toLocaleString('ru', {
			month: 'long',
			day: 'numeric'
		});
	}

	d = [
		'0' + d.getDate(),
		'0' + (d.getMonth() + 1),
		'' + d.getFullYear(),
		'0' + d.getHours(),
		'0' + d.getMinutes(),
	];
	for (var i = 0; i < d.length; i++) {
		d[i] = d[i].slice(-2);
	}
	return d.slice(0, 3).join('.') + ' ' + d.slice(3).join(':');
}


function createUUID() {
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    return s.join("");
}


function process_error(context, response, silent) {
	if (response.status == 0) { // ERR_CONNECTION_REFUSED
		Toast.create.negative({
			html: 'Отсутствует соединение с сервером. Проверьте интернет соединение и попробуйте позже',
			icon: 'alarm_add',
			timeout: 5000,
		});
	}
	else if (response.status == 403) { // FORBIDDEN
		if (response.body.detail) {
			Toast.create.negative({
				html: 'Ошибка: ' + response.body.detail,
				icon: 'alarm_add',
				timeout: 5000,
			});
		}
		else {
			Toast.create.negative({
				html: 'Доступ запрещен',
				icon: 'alarm_add',
				timeout: 5000,
			});
		}
	}
	else if (response.status == 404) { // NOT_FOUND
		if (response.body.detail) {
			Toast.create.negative({
				html: 'Ошибка 404: ' + response.body.detail,
				icon: 'alarm_add',
				timeout: 5000,
			});
		}
		else {
			Toast.create.negative({
				html: 'Ошибка 404: Не найдено',
				icon: 'alarm_add',
				timeout: 5000,
			});
		}
	}
	else if (response.status == 400) { // Bad Request
		if (response.body.non_field_errors) {
			Toast.create.negative({
				html: 'Ошибка: ' + response.body.non_field_errors.join(", "),
				icon: 'alarm_add',
				timeout: 5000,
			});
		}
		else if (response.body.detail) {
			Toast.create.negative({
				html: 'Ошибка: ' + response.body.detail,
				icon: 'alarm_add',
				timeout: 5000,
			});
		}
		else {
			Toast.create.negative({
				html: 'Ошибка введенных данных',
				icon: 'alarm_add',
				timeout: 5000,
			});
		}
	}
	else if (response.status == 401) {
		if (!silent) {
			console.error("need logout");
		}
	}
	else {
		Toast.create.negative({
			html: 'Неизвестная ошибка с кодом ' + response.status,
			icon: 'alarm_add',
			timeout: 5000,
		});
	}
}

if (localStorage.getItem('debug_mode') == null) {
	localStorage.setItem('debug_mode', '0');
}
if (localStorage.getItem('local_url') == null) {
	localStorage.setItem('local_url', '0');
}

if (localStorage.getItem('guid') && Cookies.get('guid')) {
}
else if(localStorage.getItem('guid') || sessionStorage.getItem('guid')) {
	if (localStorage.getItem('guid')) {
		sessionStorage.setItem('guid', localStorage.getItem('guid'));
		Cookies.set('guid', localStorage.getItem('guid'));
		// console.log("set by LS");
	}
	else if (sessionStorage.getItem('guid')) {
		localStorage.setItem('guid', sessionStorage.getItem('guid'));
		Cookies.set('guid', sessionStorage.getItem('guid'));
		// console.log("set by SS");
	}
}
else {
	var guid = createUUID();
	Cookies.set('guid', guid);
	localStorage.setItem('guid', guid);
	sessionStorage.setItem('guid', guid);
	//console.log("guid created");
}

if (!sessionStorage.getItem('luid')) {
	sessionStorage.setItem('luid', createUUID());
	//console.log("luid created");
}

export default {
	debug_mode: debug_mode,

	// for REST API
	API_URL: API_URL,
	LOGIN_URL: LOGIN_URL,
	INITIAL_URL: INITIAL_URL,
	LOGOUT_URL: LOGOUT_URL,

	pluralize: pluralize,
	relative_time: relative_time,
	process_error: process_error,

	logout(context) {
		context.$store.commit("LOGOUT", null, {module: 'user_app'});
	},
	user_get_short_name(self) {
		var arr = [];
		if (self.name)
			arr.push(self.name);
		if (self.last_name)
			arr.push(self.last_name);
		if (arr.length > 0)
			return arr.join(" ");
		else
			return self.username;
	},
	user_get_full_name(self) {
		var arr = [];
		if (self.last_name)
			arr.push(self.last_name);
		if (self.name)
			arr.push(self.name);
			if (self.surname)
				arr.push(self.surname);
		if (arr.length > 0)
			return arr.join(" ");
		else
			return self.username;
	},
	course_is_free(self) {
		return self.cost_coins == 0 && self.cost_cristals == 0 && self.cost_rubles == 0;
	},
	course_get_cost_string(self) {
		var arr = [];
        if (self.cost_rubles > 0)
            arr.push(pluralize(self.cost_rubles, ["рублей", "рубль", "рубля"]));
        if (self.cost_cristals > 0)
            arr.push(pluralize(self.cost_cristals, ["кристалов", "кристал", "кристала"]));
        if (self.cost_coins > 0)
            arr.push(pluralize(self.cost_coins, ["монет", "монета", "монеты"]));

        if (len(arr) == 3)
            return "за " + arr[0] + ", " + arr[1] + " и " + arr[2];
        else if (len(arr) == 2)
            return "за " + arr[0] + " и " + arr[1];
        else if (len(arr) == 1)
            return "за " + arr[0];
        console.error("cost arr has unknown length");
        return "бесплатно";
	},
}
