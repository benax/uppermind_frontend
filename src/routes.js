import Main from './components/main/Main.vue'
import Dashboard from './components/user_app/Dashboard.vue'
import Login from './components/user_app/Login.vue'
import Courses from './components/learn/Courses.vue'
import CoursePage from './components/learn/CoursePage.vue'
import CourseExecutions from './components/learn/CourseExecutions.vue'
import CourseExecutionPage from './components/learn/CourseExecutionPage.vue'
import LessonPage from './components/learn/LessonPage.vue'
import LessonExecutionPage from './components/learn/LessonExecutionPage.vue'
import TaskExecutionPage from './components/learn/TaskExecutionPage.vue'
import ReferenceCodesPage from './components/learn/ReferenceCodesPage.vue'
import СompletedCodeExecutions from './components/learn/СompletedCodeExecutions.vue'
import Error404 from './components/Error404.vue'
import MyProfile from './components/user_app/MyProfile.vue'

import TaskExecutionCheckPage from './components/admin/TaskExecutionCheckPage.vue'



export default [
	{ path: '/', component: Main, name: 'home', meta: {title: 'Главная', need_auth: false} },
	{ path: '/login', component: Login, name: 'login', meta: {title: 'Войти', need_auth: false} },
	{ path: '/dashboard', component: Dashboard, name: 'dashboard', meta: {title: 'Обзор', need_auth: true} },
	
	{ path: '/courses', component: Courses, name: 'courses', meta: {title: 'Курсы', need_auth: false} },
	{ path: '/courses/:pk', component: CoursePage, name: 'course_page', meta: {need_auth: false}},

	{ path: '/course_executions', component: CourseExecutions, name: 'course_executions', meta: {title: 'Мои курсы', need_auth: true} },
	{ path: '/course_executions/:pk', component: CourseExecutionPage, name: 'course_execution_page', meta: {need_auth: true}},
	
	//{ path: '/lessons/:pk', component: LessonPage, name: 'lesson_page', meta: {need_auth: false}},
	
	{ path: '/lesson_executions/:pk', component: LessonExecutionPage, name: 'lesson_execution_page', meta: {need_auth: true}},
	
	{ path: '/task_executions/:pk', component: TaskExecutionPage, name: 'task_execution_page', meta: {need_auth: true}},
	
	{ path: '/reference_codes/:pk', component: ReferenceCodesPage, name: 'reference_codes_page', meta: {need_auth: true}},
	
	{ path: '/completed_code_executions', component: СompletedCodeExecutions, name: 'completed_code_executions', meta: {title: 'Завершенные решения', need_auth: true} },

	{ path: '/my_profile', component: MyProfile, name: 'my_profile', meta: {title: 'Мой профиль', need_auth: true} },
	
	// admin
	{ path: '/task_check/:pk', component: TaskExecutionCheckPage, name: 'task_check_page', meta: {need_auth: true}},



	{ path: '*', component: Error404, meta: {title: "Ошибка"}},
]